# dyASMTop

## 介绍
该工具包是学习redkale字节码部分的衍生工具包。

asm使用比较麻烦，此jar包是封装asm直接生成字节码对象的工具

准备综合示例
[JFish](https://gitee.com/jlutt/JFish)

## 开发环境
> - jdk1.8 + maven
> - jdk内置asm + GeneratorAdapter方式

## 开发记录
> - **大概差不多的情况下会进行完整示例的编码**
> - **目前pom是传到的私服，编译需要去掉**

> - 2018-01-15 生成基本的骨架代码，把字节码指令抽象为command准备实现
> - 2018-01-15 完成参数调用arg和方法调用call指令
> - 2018-01-16 完成field属性获取方法
> - 2018-01-16 完成临时变量的获取local
> - 2018-01-16 完成定义临时变量的let方法
> - 2018-01-16 完成调用类构造函数的方法
> - 2018-01-16 完成比较类方法
> - 2018-01-16 完成if...else方法
> - 2018-01-16 增加注解回调方法功能
> - 2018-01-17 增加四则运算方法
> - 2018-01-17 增加调用super方法callSuper(只能调用mainClass中的方法)
> - 2018-01-17 增加调用外部类静态方法callStatic
> - 2018-01-17 增加调用静态方法callStaticSelf
> - 2018-01-17 增加for i方法
> - 2018-01-17 ClassBuilder增加buildInstance返回对象实例方法
> - 2018-01-17 增加数组new，get，set，length操作
> - 2018-01-17 增加map循环操作
> - 2018-01-17 增加数组，集合循环操作
> - 2018-01-17 修改super调用方式，在不传入方法名和参数情况下，默认为在重载方法中直接调用父类对应的方法
> - 2018-01-17 增加获取其他对象静态字段的方法fieldStatic
> - 2018-01-19 完成增加静态函数addStaticMethod
> - 2018-01-19 修复field方法中没有判断isXXX的情况
> - 2018-01-19 增加hello world示例
> - 2018-01-19 增加Bean转换到map示例
> - 2018-01-20 增加JFish，使用asmtop和antlr在jvm上实现自己的编程语言

## 简单示例


> ```
> public class FirstHelloWorld {
>
>   //要实现的代码
> //  public class FirstHelloWorld {
> //
> //    public static void main(String[] args) {
> //      System.out.println("Hello World.");
> //    }
> //
> //  }
>
>   public static void main(String... args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
>     Class<Object> testHello = ClassBuilder.create(ASMClassLoader.create(), Object.class)
>         //增加main静态方法
>         .addStaticMethod("main", void.class, Collections.singletonList(String[].class),
>             //在有多个函数情况，返回最后一个command.type()，所以在下面加了一个voidReturn()
>             CB.sequence(
>                 CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value("Hello World.")),
>                 CB.voidReturn()
>             ))
>         .build();
>
>     //没有对应的接口，用反射测试
>     Method mainMethod = testHello.getMethod("main", String[].class);
>     mainMethod.invoke(testHello, new Object[]{null});
>   }
> }
> ```


##### 反编译保存的class文件后的效果
![image](https://gitee.com/jlutt/dyASMTop/raw/master/%E5%8F%8D%E7%BC%96%E8%AF%91.png)


## 参考实现
> 1. [redkale](https://github.com/redkale/redkale)
> 1. [asmsupport](https://github.com/wensiqun/asmsupport)
> 1. [datakernel](https://github.com/softindex/datakernel)