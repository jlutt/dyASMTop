package com.dny.asmtop;

import jdk.internal.org.objectweb.asm.Type;

/**
 * Created by jlutt on 2018-01-15.
 * 方法指令
 * @author jlutt
 */
public interface Command {

  Type type(MethodContext context);

  Type generator(MethodContext context);
}
