package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;

import java.util.Objects;

/**
 * Created by jlutt on 2018-01-16.
 * 赋值操作
 * @author jlutt
 */
public class CommandSet implements Command {

  private final StoreDef to;
  private final Command from;

  public CommandSet(StoreDef to, Command from) {
    this.to = to;
    this.from = from;
  }

  @Override
  public Type type(MethodContext context) {
    return Type.VOID_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    Object storeContext = to.beginStore(context);
    Type type = from.generator(context);
    to.store(context, storeContext, type);
    return Type.VOID_TYPE;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CommandSet that = (CommandSet) o;
    return Objects.equals(to, that.to) &&
        Objects.equals(from, that.from);
  }

  @Override
  public int hashCode() {
    return Objects.hash(to, from);
  }
}
