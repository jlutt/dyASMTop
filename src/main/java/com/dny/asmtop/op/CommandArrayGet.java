package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

import java.util.Objects;

/**
 * Created by jlutt on 2018-01-17.
 *
 * @author jlutt
 */
public class CommandArrayGet implements Command {

  private Command array;
  private Command position;

  public CommandArrayGet(Command array, Command position) {
    this.array = array;
    this.position = position;
  }

  @Override
  public Type type(MethodContext context) {
    return Type.getType(array.type(context).getDescriptor().substring(1));
  }

  @Override
  public Type generator(MethodContext context) {
    Type type = type(context);

    GeneratorAdapter g = context.getGeneratorAdapter();
    array.generator(context);
    position.generator(context);
    g.arrayLoad(type);

    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CommandArrayGet that = (CommandArrayGet) o;
    return Objects.equals(array, that.array) &&
        Objects.equals(position, that.position);
  }

  @Override
  public int hashCode() {

    return Objects.hash(array, position);
  }
}
