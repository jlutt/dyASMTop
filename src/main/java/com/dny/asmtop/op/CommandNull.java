package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

import java.util.Objects;

import static jdk.internal.org.objectweb.asm.Type.getType;

/**
 * Created by jlutt on 2018-01-16.
 * 返回null
 *
 * @author jlutt
 */
public class CommandNull implements Command {

  private final Type type;

  public CommandNull(Class<?> type) {
    this.type = getType(type);
  }

  public CommandNull(Type type) {
    this.type = type;
  }

  @Override
  public Type type(MethodContext context) {
    return type;
  }

  @Override
  public Type generator(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    g.push((String) null);
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CommandNull that = (CommandNull) o;
    return Objects.equals(type, that.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type);
  }
}
