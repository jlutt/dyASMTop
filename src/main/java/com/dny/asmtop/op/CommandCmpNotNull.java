package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Label;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

import java.util.Objects;

/**
 * Created by jlutt on 2018-01-16.
 * 判断表达式不为空
 * @author jlutt
 */
public class CommandCmpNotNull implements PredicateDef {

  private final Command value;

  public CommandCmpNotNull(Command value) {
    this.value = value;
  }

  @Override
  public Type type(MethodContext context) {
    return Type.BOOLEAN_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();

    Label labelNotNull = new Label();
    Label labelExit = new Label();

    value.generator(context);
    g.ifNonNull(labelNotNull);
    g.push(false);
    g.goTo(labelExit);

    g.mark(labelNotNull);
    g.push(true);

    g.mark(labelExit);

    return Type.BOOLEAN_TYPE;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CommandCmpNotNull that = (CommandCmpNotNull) o;
    return Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }
}
