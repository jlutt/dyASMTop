package com.dny.asmtop.op;

import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

/**
 * Created by jlutt on 2018-01-16.
 * 返回true和false常量
 *
 * @author jlutt
 */
public class PredicateDefConst implements PredicateDef {

  private final boolean value;

  public PredicateDefConst(boolean value) {
    this.value = value;
  }

  @Override
  public Type type(MethodContext context) {
    return Type.BOOLEAN_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    g.push(value);
    return Type.BOOLEAN_TYPE;
  }
}
