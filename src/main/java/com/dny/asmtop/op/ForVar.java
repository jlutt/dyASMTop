package com.dny.asmtop.op;

import com.dny.asmtop.Command;

/**
 * Created by jlutt on 2018-01-17.
 * for循环操作内容
 * @author jlutt
 */
@FunctionalInterface
public interface ForVar {
  Command forLoop(Command it);
}
