package com.dny.asmtop.op;

import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;

/**
 * Created by jlutt on 2018-01-15.
 * 存储变量
 *
 * @author jlutt
 */
public interface StoreDef {

  /**
   * 获取owner类型
   *
   * @param context
   * @return
   */
  Object beginStore(MethodContext context);

  /**
   * 变量压栈操作
   *
   * @param context      方法信息
   * @param storeContext owner类型
   * @param type         参数类型
   */
  void store(MethodContext context, Object storeContext, Type type);
}
