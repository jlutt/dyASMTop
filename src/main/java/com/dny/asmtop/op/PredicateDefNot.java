package com.dny.asmtop.op;

import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Label;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
public class PredicateDefNot implements PredicateDef {

  private final PredicateDef predicateDef;

  public PredicateDefNot(PredicateDef predicateDef) {
    this.predicateDef = predicateDef;
  }

  @Override
  public Type type(MethodContext context) {
    return Type.BOOLEAN_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    Label labelFalse = g.newLabel();
    Label labelExit = g.newLabel();
    predicateDef.generator(context);
    g.ifZCmp(GeneratorAdapter.EQ, labelFalse);
    g.push(false);
    g.goTo(labelExit);
    g.visitLabel(labelFalse);
    g.push(true);
    g.visitLabel(labelExit);
    return Type.BOOLEAN_TYPE;
  }
}
