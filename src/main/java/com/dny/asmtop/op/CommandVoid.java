package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;

/**
 * Created by jlutt on 2018-01-17.
 * 返回无类型，用于进行return操作
 * @author jlutt
 */
public class CommandVoid implements Command {

  public static final CommandVoid instance = new CommandVoid();

  private CommandVoid() {

  }

  @Override
  public Type type(MethodContext context) {
    return Type.VOID_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    return Type.VOID_TYPE;
  }
}
