package com.dny.asmtop.op;

import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

import java.util.Objects;

/**
 * Created by jlutt on 2018-01-16.
 * 临时变量
 *
 * @author jlutt
 */
public class VarLocal implements Variable {

  private final int local;

  public VarLocal(int local) {
    this.local = local;
  }

  @Override
  public Type type(MethodContext context) {
    return context.getGeneratorAdapter().getLocalType(local);
  }

  @Override
  public Type generator(MethodContext context) {
    context.getGeneratorAdapter().loadLocal(local);
    return type(context);
  }

  @Override
  public Object beginStore(MethodContext context) {
    return null;
  }

  @Override
  public void store(MethodContext context, Object storeContext, Type type) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    g.storeLocal(local);
  }

  public void store(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    g.storeLocal(local);
  }

  public void storeLocal(GeneratorAdapter g) {
    g.storeLocal(local);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VarLocal varLocal = (VarLocal) o;
    return local == varLocal.local;
  }

  @Override
  public int hashCode() {
    return Objects.hash(local);
  }
}
