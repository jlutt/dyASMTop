package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;

import java.util.Objects;

/**
 * Created by jlutt on 2018-01-15.
 * 参数指令
 *
 * @author jlutt
 */
public final class VarArg implements Variable {

  private final int argument;

  public VarArg(int argument) {
    this.argument = argument;
  }

  @Override
  public Type type(MethodContext context) {
    return context.getArgumentType(argument);
  }

  @Override
  public Type generator(MethodContext context) {
    context.getGeneratorAdapter().loadArg(argument);
    return type(context);
  }

  @Override
  public Object beginStore(MethodContext context) {
    return null;
  }

  @Override
  public void store(MethodContext context, Object storeContext, Type type) {
    context.getGeneratorAdapter().storeArg(argument);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VarArg varArg = (VarArg) o;
    return argument == varArg.argument;
  }

  @Override
  public int hashCode() {
    return Objects.hash(argument);
  }
}
