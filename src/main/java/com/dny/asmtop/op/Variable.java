package com.dny.asmtop.op;

import com.dny.asmtop.Command;

/**
 * Created by jlutt on 2018-01-16.
 * 变量基类
 * @author jlutt
 */
public interface Variable extends Command, StoreDef {
}
