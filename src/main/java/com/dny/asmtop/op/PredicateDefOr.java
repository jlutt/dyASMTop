package com.dny.asmtop.op;

import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Label;
import jdk.internal.org.objectweb.asm.Type;
import jdk.internal.org.objectweb.asm.commons.GeneratorAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.EMPTY_LIST;
import static jdk.internal.org.objectweb.asm.Type.BOOLEAN_TYPE;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
public class PredicateDefOr implements PredicateDef {

  private final List<PredicateDef> predicates;

  private PredicateDefOr(List<PredicateDef> predicates) {
    this.predicates = new ArrayList<>(predicates);
  }

  public static PredicateDefOr create() {
    return new PredicateDefOr(EMPTY_LIST);
  }

  public static PredicateDefOr create(List<PredicateDef> predicates) {
    return new PredicateDefOr(predicates);
  }

  public PredicateDefOr add(PredicateDef predicateDef) {
    this.predicates.add(predicateDef);
    return this;
  }

  @Override
  public Type type(MethodContext context) {
    return BOOLEAN_TYPE;
  }

  @Override
  public Type generator(MethodContext context) {
    GeneratorAdapter g = context.getGeneratorAdapter();
    Label exit = new Label();
    Label labelTrue = new Label();
    for (PredicateDef predicate : predicates) {
      Type localType = predicate.generator(context);
      assert localType == BOOLEAN_TYPE;
      g.ifZCmp(GeneratorAdapter.NE, labelTrue);
    }
    g.push(false);
    g.goTo(exit);

    g.mark(labelTrue);
    g.push(true);

    g.mark(exit);
    return BOOLEAN_TYPE;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PredicateDefOr that = (PredicateDefOr) o;
    return Objects.equals(predicates, that.predicates);
  }

  @Override
  public int hashCode() {
    return Objects.hash(predicates);
  }
}
