package com.dny.asmtop.op;

import com.dny.asmtop.Command;
import com.dny.asmtop.MethodContext;
import jdk.internal.org.objectweb.asm.Type;

/**
 * Created by jlutt on 2018-01-16.
 * 返回this当前实例对象
 * @author jlutt
 */
public class CommandThis implements Command {

  @Override
  public Type type(MethodContext context) {
    return context.getThisType();
  }

  @Override
  public Type generator(MethodContext context) {
    context.getGeneratorAdapter().loadThis();
    return type(context);
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null || getClass() != obj.getClass()) return false;

    return true;
  }
}
