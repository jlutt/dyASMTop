package com.dny.asmtop;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
@FunctionalInterface
public interface WithValue<T> {
  T get();
}
