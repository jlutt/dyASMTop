package com.dny.asmtop;

import jdk.internal.org.objectweb.asm.FieldVisitor;

/**
 * Created by jlutt on 2018-01-16.
 * 字段注解
 * @author jlutt
 */
@FunctionalInterface
public interface FieldAnnotationWrite {
  void write(FieldVisitor fv);
}
