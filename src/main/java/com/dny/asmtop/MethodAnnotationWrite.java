package com.dny.asmtop;

import jdk.internal.org.objectweb.asm.MethodVisitor;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
@FunctionalInterface
public interface MethodAnnotationWrite {
  void write(MethodVisitor mv);
}
