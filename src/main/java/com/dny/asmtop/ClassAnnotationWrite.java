package com.dny.asmtop;

import jdk.internal.org.objectweb.asm.ClassWriter;

/**
 * Created by jlutt on 2018-01-16.
 * 类注解
 * @author jlutt
 */
@FunctionalInterface
public interface ClassAnnotationWrite {
  void write(ClassWriter cw);
}
