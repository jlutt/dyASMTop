package com.asm.test;

/**
 * Created by jlutt on 2018-01-19.
 *
 * @author jlutt
 */
public class TestPojo1 {
  private int fieldInt;
  private String fieldString;

  private long fieldLong;

  private boolean fieldBoolean;

  public int getFieldInt() {
    return fieldInt;
  }

  public void setFieldInt(int fieldInt) {
    this.fieldInt = fieldInt;
  }

  public String getFieldString() {
    return fieldString;
  }

  public void setFieldString(String fieldString) {
    this.fieldString = fieldString;
  }

  public long getFieldLong() {
    return fieldLong;
  }

  public void setFieldLong(long fieldLong) {
    this.fieldLong = fieldLong;
  }

  public boolean isFieldBoolean() {
    return fieldBoolean;
  }

  public void setFieldBoolean(boolean fieldBoolean) {
    this.fieldBoolean = fieldBoolean;
  }
}
