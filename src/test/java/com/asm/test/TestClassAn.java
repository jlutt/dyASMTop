package com.asm.test;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
@Inherited
@Documented
@Target({TYPE})
@Retention(RUNTIME)
public @interface TestClassAn {

  String name();

  String value();
}
