package com.asm.test;

/**
 * Created by jlutt on 2018-01-17.
 *
 * @author jlutt
 */
public class TestCall4 extends TestCall3 {

  @Override
  public int add(int a, int b) {
    return super.add(a, b);
  }

  public int addSelf() {
    return super.add(3,4);
  }

  public int addFor() {

    //System.out.println("aa");
    Integer result = null;

    result = 0;
//    for (int i = 0; i < 100; i++) {
//      result = result + 1;
//    }

    return result;
  }
}
