package com.asm.test;

/**
 * Created by jlutt on 2018-01-16.
 *
 * @author jlutt
 */
public class TestCall3 {

  public int add(int a, int b) {
    return a + b + 12;
  }

  public Integer addSuper(int a, int b) {
    return a + b;
  }
}
