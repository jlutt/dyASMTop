package com.asm.test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jlutt on 2018-01-19.
 *
 * @author jlutt
 */
public class BeanConvertMapTest {

  public static void main(String... args) {
    TestPojo1 pojo1 = new TestPojo1();
    pojo1.setFieldInt(1);
    pojo1.setFieldBoolean(true);
    pojo1.setFieldString("测试bean转map");
    pojo1.setFieldLong(1234L);

    BeanConvertMap<TestPojo1> convertMap = BeanConvertMap.create(TestPojo1.class);
    Map<String, Object> resultMap = new HashMap<>();

    resultMap = convertMap.apply(resultMap, pojo1);
    System.out.println(resultMap.toString());
  }
}
