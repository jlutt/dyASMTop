package com.asm.test;

import com.dny.asmtop.*;
import com.dny.asmtop.op.CommandSequence;
import com.dny.asmtop.op.VarField;
import com.dny.asmtop.op.Variable;
import jdk.internal.org.objectweb.asm.AnnotationVisitor;
import jdk.internal.org.objectweb.asm.Type;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;

/**
 * Created by jlutt on 2018-01-15.
 *
 * @author jlutt
 */
public class ASMTopTest {

  public static void sysOutPrintln(String message) {
    System.out.println(message);
  }

  public interface Test {
    int add(int a, int b);

    int addif(int a, int b);

    int addArray(TestCall2 call2, int... array);
  }

  public interface TestFor {
    int addFor();
    int addFor2();
  }

  public class TestCall2 {
    public int add2(int a, int b) {
      return a + b;
    }

    public int addArray(int... arrays) {
      return 0;
    }
  }

  @SuppressWarnings("unchecked")
  public static void main(String[] args) throws Exception {
    File file = new File("");
    Path currentPath = file.toPath();

    Class<Test> testClass = ClassBuilder.create(ASMClassLoader.create(), Test.class)
        .savePath(currentPath)
        .withAnnotation((cw) -> {
          AnnotationVisitor av0 = cw.visitAnnotation(Type.getDescriptor(TestClassAn.class), true);
          av0.visit("name", "测试注解类");
          av0.visit("value", "测试值");
          av0.visitEnd();
        })
        .addField("x", int.class)
        .addField("y", Long.class)
        .addMethod("add", ((WithValue<Command>) () -> {
          CommandSequence sequence = CommandSequence.create();
          Variable pojo = CB.let(CB.constructor(TestCall.class));
          //下面的代码由于TestCall2是内部类会报错，找不到构造函数
          //Variable pojo2 = CB.let(CB.constructor(TestCall2.class));
          sequence.add(CB.call(pojo, "add", CB.arg(0), CB.arg(1)));
          return sequence;
        }).get())
        .addMethod("addArray", CB.call(CB.arg(0), "addArray", CB.arg(1)))
        .addMethod("addif", CB.ifThenElse(
            CB.ge(CB.arg(0), CB.value(1)),

            ((WithValue<Command>) () -> {
              CommandSequence sequence = CommandSequence.create();
              Variable pojo = CB.let(CB.constructor(TestCall.class));
              sequence.add(CB.call(pojo, "add", CB.arg(0), CB.arg(1)));
              return sequence;
            }).get(),

            ((WithValue<Command>) () -> {
              CommandSequence sequence = CommandSequence.create();
              Variable pojo = CB.let(CB.constructor(TestCall3.class));
              sequence.add(CB.call(pojo, "add", CB.arg(0), CB.arg(1)));
              return sequence;
            }).get()))
        .build();

    Variable result = CB.let(CB.value(0));
    Class<TestCall3> testChildClass = ClassBuilder.create(ASMClassLoader.create(), TestCall3.class)
        .savePath(currentPath)
        .addMethod("add", CB.callSuper("add", CB.arg(0), CB.arg(1)))
        .addMethod("addFor", int.class, Collections.emptyList(), CB.sequence(
            result,
            CB.fori(CB.value(0), CB.value(100), (lt) -> {
              //return CB.set(result, CB.add(result, CB.value(1)));
              return CB.set(result, CB.add(result, lt));
            }),
            result
        ))
        .build();


    TestFor testFor = ClassBuilder.create(ASMClassLoader.create(), TestFor.class)
        .savePath(currentPath)
        .addMethod("addFor", ((WithValue<Command>) () -> {
          Variable fortemp = CB.let(CB.value(0));
          return CB.sequence(
              fortemp,
              CB.fori(CB.value(0), CB.value(100), (lt) -> {
                //return CB.set(result, CB.add(result, CB.value(1)));
                return CB.set(fortemp, CB.add(fortemp, lt));
              }),
              fortemp
          );
        }).get())
        .addMethod("addFor2", ((WithValue<Command>) () -> {
          Variable fortemp = CB.let(CB.value(0));
          return CB.sequence(
              fortemp,
              CB.fori(CB.value(0), CB.value(100), (lt) -> {
                return CB.set(fortemp, CB.add(fortemp, CB.value(1)));
              }),
              fortemp
          );
        }).get())
        .buildInstance();

    TestCall3 testSuper = ClassBuilder.create(ASMClassLoader.create(), TestCall3.class)
        .savePath(currentPath)
        .addMethod("addSuper", ((WithValue<Command>) () -> {
          //如果addSuper返回非基础类型，则可以使用nullRef，否则会报引用类型错误
          //在这里如果有全局代理的话，需要进行配置
          Variable itemp = CB.let(CB.nullRef(Object.class));
//          Variable itemp = CB.let(CB.value(0));
          return CB.sequence(
              itemp,
              CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value("直接调用System.out开始111")),
              CB.callStatic(ASMTopTest.class, "sysOutPrintln", CB.value("调用方法开始")),
              CB.set(itemp, CB.callSuper()),
              CB.callStatic(ASMTopTest.class, "sysOutPrintln", CB.value("调用方法结束")),
              CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value("直接调用System.out结束111")),
              CB.cast(itemp, Integer.class)
              //itemp
          );
        }).get())
        .buildInstance();


//    System.out.println(testFor.addFor());
//    System.out.println(testFor.addFor2());

    System.out.println(testSuper.addSuper(1,2));
  }
}
