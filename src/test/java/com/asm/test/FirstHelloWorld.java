package com.asm.test;

import com.dny.asmtop.ASMClassLoader;
import com.dny.asmtop.CB;
import com.dny.asmtop.ClassBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;

/**
 * Created by jlutt on 2018-01-18.
 *
 * @author jlutt
 */
public class FirstHelloWorld {

  //要实现的代码
//  public class FirstHelloWorld {
//
//    public static void main(String[] args) {
//      System.out.println("Hello World.");
//    }
//
//  }

  public static void main(String... args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    Class<Object> testHello = ClassBuilder.create(ASMClassLoader.create(), Object.class)
        //增加main静态方法
        .addStaticMethod("main", void.class, Collections.singletonList(String[].class),
            //在有多个函数情况，返回最后一个command.type()，所以在下面加了一个voidReturn()
            CB.sequence(
                CB.call(CB.fieldStatic(System.class, "out"), "println", CB.value("Hello World.")),
                CB.voidReturn()
            ))
        .build();

    //没有对应的接口，用反射测试
    Method mainMethod = testHello.getMethod("main", String[].class);
    mainMethod.invoke(testHello, new Object[]{null});
  }
}
